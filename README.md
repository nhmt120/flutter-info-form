# info_form

Viết Flutter app có form nhập liệu thông tin người dùng gồm:
* Email address
* Họ & chữ lót
* Tên
* Ngày sinh nhật:
Yêu cầu tất cả field đều phải nhập; bạn tự đề xuất yêu cầu validation cho từng field và cài đặt.

**Mở rộng**: Thêm mấy field này (with constraints)
* Địa chỉ: gồm các trường lựa chọn bao gồm:
  - Quốc gia: Mặc định là chọn Việt Nam
  - Tỉnh/Thành phố:
  - Quận/huyện.
  (Các bạn tham khảo các ứng dụng phổ biến như: Tiki, Lazada,...)
  - (Optional): Khi chọn Quốc gia (vd: Viet Name); thì các giá trị Tỉnh/Thành phố chỉ hiển thị trong Việt Nam để người dụng chọn.
