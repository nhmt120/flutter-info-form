mixin CommonValidation {
  String? validateEmail(String? value) {
    if (!value!.contains('@')) {
      return 'Please enter valid email. Must contain \'@\'.';
    } else if (!value.contains('.')) {
      return 'Please enter valid email. Must contain \'.\'.';
    }
    return null;
  }

  String? validateLastName(String? value) {
    if (value!.length < 2) {
      return 'Last name must contain at least 2 characters.';
    }
    return null;
  }

  String? validateFirstName(String? value) {
    if (value!.length < 3) {
      return 'First name must contain at least 3 characters.';
    }
    return null;
  }

  String? validateYear(String? value) {
    if (int.parse(value!) < 1900) {
      return 'You are definitely not born before the 1900.';
    }
    return null;
  }

  String? validateAddress(String? value) {
    if (value!.length < 5) {
      return 'Address must contain at least 5 characters.';
    }
    return null;
  }

  String? validateCountry(String? value) {
    if (value! == '') {
      return 'Must select a country.';
    }
    return null;
  }

  String? validateCity(String? value) {
    if (value! == '') {
      return 'Must select a city.';
    }
    return null;
  }

  String? validateDistrict(String? value) {
    if (value! == '') {
      return 'Must select a district.';
    }
    return null;
  }

}