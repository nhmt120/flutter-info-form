
import 'package:flutter/material.dart';
import '../validation/mixins_validation.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Submit Information",
      home: Scaffold(
        appBar: AppBar(title: const Text('User Information')),
        body: const LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String firstName;
  late String lastName;
  late String year;
  late String address;
  late String password;
  late String country = '';
  late String city = '';
  late String district = '';


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            fieldEmailAddress(),
            Container(margin: const EdgeInsets.only(top: 10.0)),
            fieldFirstName(),
            Container(margin: const EdgeInsets.only(top: 10.0)),
            fieldLastName(),
            Container(margin: const EdgeInsets.only(top: 10.0)),
            fieldYear(),
            Container(margin: const EdgeInsets.only(top: 10.0)),
            fieldAddress(),
            Container(margin: const EdgeInsets.only(top: 10.0),),
            fieldCountry(),
            Container(margin: const EdgeInsets.only(top: 10.0),),
            fieldCity(),
            Container(margin: const EdgeInsets.only(top: 10.0),),
            fieldDistrict(),
            Container(margin: const EdgeInsets.only(top: 10.0),),
            submitButton()
          ],
        ),
      )
    );
  }

  List<DropdownMenuItem<String>>? dropDistrict(String country, String city) {
    late List<String> districts;

    if (country == 'Vietnam') {
      if (city == 'Ho Chi Minh City') {
        districts = ['', 'District 1', 'District 2', 'District 3', 'District 4', 'District 5', 'District 6', 'District 7', 'District 8', 'District 9', 'District 10', 'District 11', 'District 12', 'Tan Binh District', 'Tan Phu District'];
      } else if (city == 'Hanoi') {
        districts = ['','Hoan Kiem', 'Ba Dinh', 'Dong Da', 'Hai Ba Trung'];
      } else {
        districts = [''];
      }
    } else if (country == 'Korea') {
      if (city == 'Seoul') {
        districts = ['', 'Gangnam-gu', 'Gangdong-gu', 'Mapo-gu', 'Yangcheon-gu'];
      } else if (city == 'Jeju') {
        districts = ['','Aeweol-eup', 'Jocheon‑eup', 'Hanrim‑eup'];
      } else {
        districts = [''];
      }
    } else if (country == 'Japan') {
      if (city == 'Tokyo') {
        districts = ['', 'Akihabara', 'Harajuku', 'Shinjuku', 'Roppongi', 'Ginza'];
      } else if (city == 'Osaka') {
        districts = ['','Kuzuha', 'Minamikawachi', 'Senboku', 'Sennan', 'Toyono'];
      } else if (city == 'Hiroshima') {
        districts = ['','Aki', 'Jinseki', 'Toyota'];
      } else {
        districts = [''];
      }
    } else {
      districts = [''];
    }


    return districts.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    }).toList();
  }

  List<DropdownMenuItem<String>>? dropCity(String country) {
    late List<String> cities;
    if (country == 'Vietnam') {
      cities = ['','Ho Chi Minh City', 'Hanoi'];
    } else if (country == 'Korea') {
      cities = ['','Seoul', 'Jeju', 'Incheon'];
    } else if (country == 'Japan') {
      cities = ['','Tokyo', 'Osaka', 'Hiroshima'];
    } else if (country == 'China') {
      cities = ['','Beijing', 'Shanghai', 'Wanzhou'];
    } else {
      cities = [''];
    }
    return cities.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    }).toList();
  }

  Widget fieldDistrict() {
    return SizedBox(
        height: 50,
        child: FormField(
            validator: validateDistrict,
            builder: (FormFieldState<String> state) {
              return InputDecorator(
                  isEmpty: district == '',
                  decoration: const InputDecoration(
                    icon: Icon(Icons.traffic),
                    labelText: 'District',
                    border: OutlineInputBorder(),
                  ),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        value: district,
                        icon: const Icon(Icons.arrow_drop_down),
                        items: dropDistrict(country, city),
                        onChanged: (String? newValue) {
                          setState(() {
                            district = newValue!;
                          });
                        },
                      )
                  )
              );
            }
        )
    );
  }

  Widget fieldCity() {
    return SizedBox(
        height: 50,
        child: FormField(
            validator: validateCity,
            builder: (FormFieldState<String> state) {
              return InputDecorator(
                  isEmpty: city == '',
                  decoration: const InputDecoration(
                    icon: Icon(Icons.location_city),
                    labelText: 'City',
                    border: OutlineInputBorder(),
                  ),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        value: city,
                        icon: const Icon(Icons.arrow_drop_down),
                        items: dropCity(country),
                        onChanged: (String? newValue) {
                          setState(() {
                            city = newValue!;
                            district = '';
                          });
                        },
                      )
                  )
              );
            }
        )
    );
  }

  Widget fieldCountry() {
    return SizedBox(
        height: 50,
        child: FormField(
            validator: validateCountry,
            builder: (FormFieldState<String> state) {
              return InputDecorator(
                  isEmpty: country == '',
                  decoration: const InputDecoration(
                    icon: Icon(Icons.public),
                    labelText: 'Country',
                    border: OutlineInputBorder(),
                  ),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        value: country,
                        icon: const Icon(Icons.arrow_drop_down),
                        items: <String>['','Vietnam', 'Korea', 'Japan']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        onChanged: (String? newValue) {
                          setState(() {
                            district = '';
                            city = '';
                            country = newValue!;
                          });
                        },
                      )
                  )
              );
            }
        )
    );
  }

  Widget submitButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('-----Submitted-----\n '
                'Email: $emailAddress\n First Name: $firstName\n Last Name: '
                '$lastName\n Birth Year: $year\n Address: $address \n Country: '
                '$country \n City: $city \n District: $district');
          }
        },
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50.0),
        ),
      ),
      child: const Padding(
          padding: EdgeInsets.all(15.0),
          child: Text('Submit',
            style: TextStyle(fontSize: 17))
      ),
    );
  }

  Widget fieldAddress() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.house),
        labelText: 'Address',
        border: OutlineInputBorder(),
      ),
      validator: validateAddress,
      onSaved: (value) {
        address = value as String;
      },
    );
  }

  Widget fieldYear() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.cake),
        labelText: 'Birth Year',
        border: OutlineInputBorder(),
      ),
      validator: validateYear,
      onSaved: (value) {
        year = value as String;
      },
    );
  }

  Widget fieldLastName() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.abc),
        labelText: 'Last Name',
        border: OutlineInputBorder(),
      ),
      validator: validateLastName,
      onSaved: (value) {
        lastName = value as String;
      },
    );
  }

  Widget fieldFirstName() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.abc),
        labelText: 'First Name',
        border: OutlineInputBorder(),
      ),
      validator: validateFirstName,
      onSaved: (value) {
        firstName = value as String;
      },
    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: const InputDecoration(
        icon: Icon(Icons.email),
        labelText: 'Email address',
        border: OutlineInputBorder(),
      ),
      validator: validateEmail,
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

}